<?php


namespace Business;


class DB
{

    private $dbh;
    private $dbName = 'import';
    private $dbUser = 'user';
    private $dbPassword = 'pass';

    private function __construct()
    {
        $this->dbh = new \mysqli('localhost', $this->dbUser, $this->dbPassword, $this->dbName);
    }

    public static function instance()
    {
        static $db = null;
        if (!$db) {
            $db = new DB();
        }
        return $db;
    }

    public function dbh()
    {
        return $this->dbh;
    }



    public function __destruct()
    {
        $this->dbh->close();
    }
}

