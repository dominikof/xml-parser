<?php


namespace Business;


use Business\DB;
use Business\Parser\IParser;
use Business\Parser\Results\DB\Table;
use Business\Parser\Results\IResult;

class Import
{

    private $importFormats = ['Ceneo', 'Nokaut'];
    private $format, $filePath;

    public function __construct($format, $filePath)
    {
        $this->format = ucfirst($format);
        $this->filePath = $filePath;
        if (!in_array($this->format, $this->importFormats))
            throw new \LogicException('Wrong import format');
    }

    public function start(IParser $parser = null, IResult $result = null)
    {
        if (empty($parser))
            $parser = $this->getParser();
        if (empty($result))
            $result = $this->getResultDB();
        $parser->parse($result);
    }

    protected function getParser()
    {
        $strat = 'Business\Parser\\' . $this->format;
        if (!class_exists($strat))
            throw new \LogicException('No parser for this format');
        return new $strat($this->filePath);
    }

    protected function getResultDB()
    {
        $strat = 'Business\Parser\Results\DB\\'.$this->format.'\DB';
        if (!class_exists($strat))
            throw new \LogicException('No result adapter for this format');
        return new $strat(
            DB::instance()->dbh(),
            new Table(DB::instance()->dbh(), 'products')
        );
    }

}