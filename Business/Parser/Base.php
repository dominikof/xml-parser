<?php

namespace Business\Parser;


use Business\Parser\Results\IResult;

abstract class Base implements IParser
{
    protected $file;
    protected $reader;
    protected $itemStruct = [];
    protected $itemName;
    const NODE_OPEN = 1, NODE_CLOSE = 15;

    public function __construct($file)
    {
        $this->file = $file;
        $this->reader = new \XMLReader();
    }

    public function parse(IResult $result)
    {
        if (!$this->reader->open($this->file))
            throw new \LogicException('Open file xml Error');

        $itemRead = new \SplStack();

        $itemRead->push($this->itemStruct);
        $item = [];
        while ($this->reader->read()) {
            if ($this->reader->nodeType == static::NODE_OPEN) {
                if (isset($itemRead->top()[$this->reader->name])) {
                    $itemRead->push($itemRead->top()[$this->reader->name]);

                    if (isset($itemRead->top()['<attr>']))
                        $this->getFromAttr($item[$this->reader->name], $itemRead->top()['<attr>']);
                    if (isset($itemRead->top()['#text']))
                        $item[$this->reader->name] = $this->reader->readInnerXml();

                }
            }
            if ($this->reader->nodeType == static::NODE_CLOSE) {
                if ($itemRead->top()['<item>'] == $this->reader->name)
                    $itemRead->pop();
                if ($this->reader->name == $this->itemName)
                    $result->save($item);
                continue;
            }
        }
    }

    private function getFromAttr(&$save, &$attrs)
    {
        for ($i = 0; $i < count($attrs); $i++) {
            $save[$attrs[$i]] = $this->reader->getAttribute($attrs[$i]);
        }
    }
}