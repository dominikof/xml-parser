<?php

namespace Business\Parser\Results\DB\Nokaut;

use Business\Parser\Results\DB\DB AS DBBase;

class DB extends DBBase
{

    public function save(array &$item)
    {
        parent::_save($item['name'], $item['description'], $item['url']);
    }
}