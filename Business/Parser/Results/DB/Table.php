<?php


namespace Business\Parser\Results\DB;


class Table {

    private $tableName;
    private $dbh;
    private $sql;
    private $sqlData;

    public function __construct(\mysqli $dbh, $tableName)
    {
        $this->dbh = $dbh;
        $this->tableName = $this->dbh->escape_string($tableName);
        $this->createTable();
        $this->sql = "INSERT INTO `$this->tableName` (`id`,`name`,`description`,`url`) VALUES :val ON DUPLICATE KEY UPDATE `name`=VALUES(`name`), `description`=VALUES(`description`);";
    }

    public function prepareData(&$name, &$description, &$url)
    {
        if (!empty($this->sqlData))
            $this->sqlData .= ',';
        else $this->sqlData = '';

        $this->sqlData .= '(null,\'' . $this->dbh->real_escape_string($name)
            . '\',\'' . $this->dbh->real_escape_string($description)
            . '\',\'' . $this->dbh->real_escape_string($url) . '\')';
    }

    public function execute()
    {
        if ($this->dbh->query($this->prepareReq()) !== false) {
            unset($this->tableName);
        } else {
            die('insert/update request error (' . $this->dbh->errno() . ') :' . print_r($this->dbh->error(), 1));
        }
    }

    private function prepareReq()
    {
        return str_replace(':val', $this->sqlData, $this->sql);
    }

    public function __destruct()
    {
        if (!empty($this->sqlData)) {
            $this->dbh->query($this->prepareReq());
        }
    }

    private function createTable()
    {
        printf('Create table `%s` %s', $this->tableName, PHP_EOL);
        $this->dbh->query("
            CREATE TABLE IF NOT EXISTS `$this->tableName` (
                `name` VARCHAR(50) NULL,
                `url` VARCHAR(200) NULL,
                `description` TEXT NULL,
                `id` INT NULL AUTO_INCREMENT,
                PRIMARY KEY  (`id`),
                UNIQUE INDEX `url` (`url`)
            )
            COLLATE='latin1_swedish_ci'
            ENGINE=InnoDB;
        ");
    }
}