<?php

namespace Business\Parser\Results\DB\Ceneo;

use Business\Parser\Results\DB\DB as DBBase;

class DB extends DBBase
{

    public function save(array &$item)
    {
        parent::_save($item['name'], $item['description'], $item['o']['url']);
    }
}