<?php

namespace Business\Parser\Results\DB;

use Business\Parser\Results\IResult;

abstract class DB implements IResult
{

    private $dbh;
    private $batch = 1000;
    private $table;

    public function __construct(\mysqli $dbh, Table $table, $batch = 1000)
    {
        $this->dbh = $dbh;
        $this->batch = $batch;
        $this->table = $table;
    }

    protected function _save(&$name, &$description, &$url)
    {
        static $num = 0;
        $this->table->prepareData($name, $description, $url);
        $num++;
        if ($this->batch == $num) {
            $this->table->execute();
            $num = 0;
        }

    }
}