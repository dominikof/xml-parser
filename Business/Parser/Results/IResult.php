<?php

namespace Business\Parser\Results;

interface IResult
{
    public function save(array &$item);
}