<?php

namespace Business\Parser\Results;

class Console implements IResult
{

    public function save(array &$item)
    {
        echo print_r($item), PHP_EOL;
    }
}