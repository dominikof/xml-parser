<?php

namespace Business\Parser;

use Business\Parser\Results\IResult;

interface IParser
{
    public function __construct($file);

    public function parse(IResult $result);
}