<?php

namespace Business\Parser;

use Business\Parser\Results\IResult;

class Ceneo extends Base
{

    protected $itemStruct = [
        'offers' => [
            '<item>' => 'offers',
            'group' => [
                '<item>' => 'group',
                '<attr>' => ['name'],
                'o' => [
                    '<item>' => 'o',
                    '<attr>' => ['url'],
                    'name' => [
                        '<item>' => 'name',
                        '#text' => '',
                    ],
                    'desc' => [
                        '<item>' => 'desc',
                        '#text' => '',
                    ],
                ]
            ]
        ]
    ];

    protected $itemName = 'o';


}