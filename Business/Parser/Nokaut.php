<?php

namespace Business\Parser;

use Business\Parser\Results\IResult;

class Nokaut extends Base
{

    protected $itemStruct = [
        'nokaut' => [
            '<item>' => 'nokaut',
            'offers' => [
                '<item>' => 'offers',
                'offer' => [
                    '<item>' => 'offer',
                    'name' => [
                        '<item>' => 'name',
                        '#text' => '',
                    ],
                    'description' => [
                        '<item>' => 'description',
                        '#text' => '',
                    ],
                    'url' => [
                        '<item>' => 'url',
                        '#text' => '',
                    ],
                ]
            ]
        ]
    ];

    protected $itemName = 'offer';
}